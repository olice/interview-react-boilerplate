# Olice Front End Engineer Candidate Interview - React Starter

## Getting Started

`$ npm install` or `yarn`

## Folder Structure
For the project to build, **these files must exist with exact filenames**:

* `public/index.html` is the page template;
* `src/index.js` is the JavaScript entry point.

All components are located in `./src`.

All application state and event handlers are stored in the `<Root />` component.

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).


## Tasks

For each of these tasks please take your time and explain your thought process as you go. We are not looking for you to get things perfect (or even right!) we are just interested to see how you go about approaching problems. For any of the tasks, feel free to use Google, StackOverflow etc!

![image](./design.png)

1. Create your own branch of this repo, the title of the branch should be your name.

2. Switch to the branch.

3. One of the strengths of React is being able to create resusable components, are there any part of this app that could be abstracted into their own components? If so, go ahead and create these!

4. Style the app to match the image above. Colours can be found at: https://flatuicolors.com/.

5. Make sure the layout works on mobile/tablet!

6. Currently you can only 'complete' a todo, what logic can we add/amend to enable 'uncompleting' a todo?

7. Commit your work and push your branch to the origin.

8. You can check out how we solved the problems by switch to the 'styled' branch. These are by no means the only/best way to do it! But take a look and compare them to your solutions.
