import React from 'react';

import Todo from './Todo';

export default function Root(props) {
  const todosItems = props.todos.map(todo => (
    <Todo
      completed={todo.completed}
      item={todo.item}
      key={todo.item}
      handleClick={props.handleClick}
    />));

  return (
    <ul>
      {todosItems}
    </ul>
  );
}
