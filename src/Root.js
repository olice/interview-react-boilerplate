import React from 'react';

import TodoList from './TodoList';
import AddTodo from './AddTodo';

class Root extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      inputValue: '',
      todos: [
        {
          item: 'Clean car',
          completed: false,
        },
        {
          item: 'Wash hair',
          completed: false,
        },
      ],
    };
  }

  handleChange = (event) => {
    this.setState({ inputValue: event.target.value });
  }

  handleSubmit = (event) => {
    const stateCopy = Object.assign({}, this.state);
    stateCopy.inputValue = '';
    stateCopy.todos = stateCopy.todos.slice();
    stateCopy.todos.push({
      item: this.state.inputValue,
      completed: false,
    });

    this.setState(stateCopy);

    event.preventDefault();
  }

  handleClick = (item, event) => {
    const index = this.state.todos.findIndex(element => element.item === item);

    const stateCopy = Object.assign({}, this.state);
    stateCopy.todos = stateCopy.todos.slice();
    stateCopy.todos[index] = Object.assign({}, stateCopy.todos[index]);
    stateCopy.todos[index].completed = true;

    this.setState(stateCopy);

    event.preventDefault();
  }

  render() {
    return (
      <div>
        <h1>Olice Todo App</h1>
        <TodoList
          todos={this.state.todos}
          handleClick={this.handleClick}
        />
        <AddTodo
          inputValue={this.state.inputValue}
          handleChange={this.handleChange}
          handleSubmit={this.handleSubmit}
        />
      </div>
    );
  }
}

export default Root;
