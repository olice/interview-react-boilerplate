import React from 'react';

class Todo extends React.Component {
  handleClick = (event) => {
    this.props.handleClick(this.props.item, event);
  }

  render() {
    const todoStyle = this.props.completed ? {
      textDecoration: 'line-through',
    } : null;

    return (
      <li style={todoStyle}>{this.props.item} <button onClick={this.handleClick}>Complete</button></li>
    );
  }
}


export default Todo;
