import React from 'react';

export default function AddTodo(props) {
  return (
    <form onSubmit={props.handleSubmit}>
      <input
        id="add"
        type="text"
        value={props.inputValue}
        onChange={props.handleChange}
      />
      <label htmlFor="add">Add todo:</label>
      <input type="submit" value="Submit" />
    </form>
  );
}
